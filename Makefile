##
## Makefile for minecraft++ in /home/thepatriot/thepatriotsrepo/minecraft++
## 
## Made by bertho_d
## Login   <bertho_d@epitech.net>
## 
## Started on  Fri Jul 25 03:24:57 2014 bertho_d
## Last update Wed Jan  3 02:00:40 2018 Lucas
##

NAME		= jeu

SRCDIR		= src/

SRC		= $(SRCDIR)main.cpp

OBJ		= $(SRC:.cpp=.o)

CFLAGS		+= -O3
CFLAGS		+= -W
CFLAGS		+= -Wall
CFLAGS		+= -Wextra
CFLAGS		+= -I$(INCLDIR)
CFLAGS		+= -I$(INCLIB)
CFLAGS		+= -std=c++11

CC		= g++
RM		= rm -f

all: $(NAME)

$(NAME): $(OBJ)
	$(CC) $(OBJ) -o $(NAME)

clean:
	$(RM) $(OBJ)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.cpp.o:
	$(CC) -c -o $@ $< $(CFLAGS)
