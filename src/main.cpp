/*
** main.cpp for  in /home/thepatriot/thepatriotsrepo/perso/codingclub
**
** Made by Alexis Bertholom
** Login   bertho_d
** Email   <alexis.bertholom@epitech.eu>
**
** Started on  Tue Jan 27 14:13:06 2015 Alexis Bertholom
// Last update Fri Jan 19 23:44:18 2018 Lucas
*/

#include <iostream>

int	main()
{
	int nb = 5;

	std::cout << nb << std::endl;
	return (0);
}
